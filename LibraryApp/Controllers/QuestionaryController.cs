﻿using LibraryApp.Models.Data_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class QuestionaryController : Controller
    {
        // GET: Questionary
        public ActionResult Index()
        {
            return View("Index");
        }
        
        [HttpPost]
        public ActionResult Index(Questionary questionary )
        {
            if (questionary.JS != "true" && questionary.HTML != "true" && questionary.CSS != "true")
                ModelState.AddModelError("", "You have not enough skills (at least 1)");

            if (ModelState.IsValid)
                return View("QuestionaryResult", questionary);
            else
                return View();
        }
    }
}