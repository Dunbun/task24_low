﻿using LibraryApp.Models;
using LibraryApp.Models.Data_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryApp.Controllers
{
    public class MainPageController : Controller
    {
        // GET: MainPage
        public ActionResult Index()
        {
            var articles = new Articles();

            return View("Index", articles);
        }
    }
}