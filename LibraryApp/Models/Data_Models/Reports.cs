﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.Data_Models
{
    public class Reports
    {
        public ICollection<Report> List { get; set; } = new List<Report> {
            new Report
            {
                AuthorName = "A. Smith",
                Date = DateTime.Now.ToString(),
                ReportText = "Report 1 Some text"
            },
            new Report
            {
                AuthorName = "B. Roman",
                Date = DateTime.Now.ToString(),
                ReportText = "Report 2 Some text"
            }
        };

        public Report this[int i]
        {
            get => List.ElementAt(i);

            set => List.Add(value);
        }
    }
}