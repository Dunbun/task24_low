﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.Data_Models
{
    public class Questionary
    {
        [Required(ErrorMessage = "Enter User email")]
        public string UserEmail { get; set; }

        [Required(ErrorMessage = "Enter User name")]
        public string UserName { get; set; }
        public string HTML { get; set; }
        public string CSS { get; set; }
        public string JS { get; set; }
        public string Gender { get; set; }
    }
}