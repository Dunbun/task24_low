﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.Data_Models
{
    public class Article
    {
        public string ArticleName { get; set; }

        public string PublicationDate { get; set; }

        public string ArticleText { get; set; }
    }
}