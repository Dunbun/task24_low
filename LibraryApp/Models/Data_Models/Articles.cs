﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryApp.Models.Data_Models
{
    public class Articles
    {
        public ICollection<Article> List { get; set; } = new List<Article> {
            new Article
            {
                ArticleName = "Керування погодою і кліматом",
                PublicationDate = "15/01/2020",
                ArticleText = "Клімат — це багаторічний режим погоди, який спостерігається вданій місцевості."
            },
            new Article
            {
                ArticleName = "Вивчення будови землі",
                PublicationDate = "20/04/2019",
                ArticleText = "Склад і будова глибинних оболонок Землі в останні десятиліття продовжують залишатися однією з найбільш інтригуючих проблем сучасної геології. Адже Земля володіє однією унікальною особливістю – на ній є життя. Тому всебічне вивчення Землі має величезне значення для людства."
            }
        };
    }
}